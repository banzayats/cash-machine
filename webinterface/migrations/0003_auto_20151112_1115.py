# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webinterface', '0002_auto_20151112_1101'),
    ]

    operations = [
        migrations.AlterField(
            model_name='card',
            name='hashed',
            field=models.CharField(max_length=128, verbose_name='Hashed'),
        ),
        migrations.AlterField(
            model_name='card',
            name='pin',
            field=models.CharField(max_length=128, verbose_name='Card pincode'),
        ),
    ]
