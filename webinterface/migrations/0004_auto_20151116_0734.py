# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django_extensions.db.fields.encrypted


class Migration(migrations.Migration):

    dependencies = [
        ('webinterface', '0003_auto_20151112_1115'),
    ]

    operations = [
        migrations.AlterField(
            model_name='operations',
            name='card',
            field=django_extensions.db.fields.encrypted.EncryptedCharField(max_length=128, verbose_name='Card bumber'),
        ),
    ]
