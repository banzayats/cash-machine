# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django_extensions.db.fields.encrypted


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Card',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('num', django_extensions.db.fields.encrypted.EncryptedCharField(max_length=128, verbose_name='Card bumber')),
                ('pin', django_extensions.db.fields.encrypted.EncryptedCharField(max_length=128, verbose_name='Card pincode')),
                ('pin_attempts', models.SmallIntegerField(default=0, verbose_name='PIN attempts')),
                ('pin_block', models.BooleanField(default=False, verbose_name='PIN block')),
                ('balance', models.DecimalField(verbose_name='Card balance', max_digits=19, decimal_places=2)),
                ('last_modified', models.DateTimeField(auto_now=True, verbose_name='Last modified')),
            ],
            options={
                'verbose_name': 'Card',
                'verbose_name_plural': 'Cards',
            },
        ),
        migrations.CreateModel(
            name='Opcodes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('description', models.TextField(verbose_name='Description', blank=True)),
            ],
            options={
                'verbose_name': 'Operation code',
                'verbose_name_plural': 'Operation codes',
            },
        ),
        migrations.CreateModel(
            name='Operations',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateField(auto_now_add=True, verbose_name='Timestamp')),
                ('summ', models.DecimalField(null=True, verbose_name='Operation ammount', max_digits=19, decimal_places=2, blank=True)),
                ('card', models.ForeignKey(verbose_name='Card', to='webinterface.Card')),
                ('code', models.ForeignKey(verbose_name='Operation code', to='webinterface.Opcodes')),
            ],
            options={
                'verbose_name': 'Operation',
                'verbose_name_plural': 'Operations',
            },
        ),
    ]
