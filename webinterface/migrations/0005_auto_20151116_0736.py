# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webinterface', '0004_auto_20151116_0734'),
    ]

    operations = [
        migrations.AlterField(
            model_name='operations',
            name='code',
            field=models.CharField(max_length=32, verbose_name='Operation code'),
        ),
        migrations.DeleteModel(
            name='Opcodes',
        ),
    ]
