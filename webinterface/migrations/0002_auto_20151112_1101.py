# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webinterface', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='card',
            old_name='num',
            new_name='number',
        ),
        migrations.AddField(
            model_name='card',
            name='hashed',
            field=models.CharField(default='111', max_length=255, verbose_name='Hashed'),
            preserve_default=False,
        ),
    ]
