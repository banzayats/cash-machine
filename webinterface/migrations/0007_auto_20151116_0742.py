# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webinterface', '0006_auto_20151116_0738'),
    ]

    operations = [
        migrations.RenameField(
            model_name='operations',
            old_name='summ',
            new_name='sum',
        ),
    ]
