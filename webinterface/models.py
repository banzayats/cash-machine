from django.db import models
from django_extensions.db.fields.encrypted import EncryptedCharField
from django.contrib.auth.hashers import make_password
from django.utils.translation import ugettext_lazy as _
from django.conf import settings


class Card(models.Model):
    number = EncryptedCharField(verbose_name=_('Card bumber'),
                             max_length=128)
    hashed = models.CharField(verbose_name=_('Hashed'),
                              max_length=128)
    pin = models.CharField(verbose_name=_('Card pincode'),
                             max_length=128)
    pin_attempts = models.SmallIntegerField(verbose_name=_('PIN attempts'),
                                            default=0)
    pin_block = models.BooleanField(verbose_name=_('PIN block'),
                                    default=False)
    balance = models.DecimalField(verbose_name=_('Card balance'),
                                  max_digits=19,
                                  decimal_places=2)
    last_modified = models.DateTimeField(verbose_name=_('Last modified'),
                                         auto_now=True)

    def save(self, *args, **kwargs):
        self.hashed = make_password(self.number, salt=settings.SECRET_KEY)
        self.pin = make_password(self.pin, salt=settings.SECRET_KEY)
        super(Card, self).save(*args, **kwargs)

    def  __unicode__(self):
        return str(self.id)

    class Meta:
        verbose_name = _('Card')
        verbose_name_plural = _('Cards')


class Operations(models.Model):
    # card = models.ForeignKey('Card', verbose_name=_('Card'))
    card = EncryptedCharField(verbose_name=_('Card bumber'),
                              max_length=128)
    timestamp = models.DateTimeField(verbose_name=_('Timestamp'),
                                 auto_now_add=True)
    code =  models.CharField(verbose_name=_('Operation code'),
                             max_length=32)
    sum = models.DecimalField(verbose_name=_('Operation ammount'),
                               max_digits=19,
                               decimal_places=2,
                               null=True,
                               blank=True)

    def  __unicode__(self):
        return str(self.id)

    class Meta:
        verbose_name = _('Operation')
        verbose_name_plural = _('Operations')