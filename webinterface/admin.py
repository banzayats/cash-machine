from django.contrib import admin
from webinterface.models import Card, Operations


class CardAdmin(admin.ModelAdmin):
    list_display = ('id', 'balance', 'pin_block', 'last_modified')
    readonly_fields = ('hashed',)


class OperationsAdmin(admin.ModelAdmin):
    list_display = ('id', 'code', 'sum', 'timestamp')


admin.site.register(Card, CardAdmin)
admin.site.register(Operations, OperationsAdmin)
