from django.views.generic.edit import FormView
from django.views.generic.base import TemplateView
from django.views.generic import View
from django.shortcuts import redirect
from django.contrib.auth.hashers import make_password
from webinterface.models import Card, Operations
from webinterface.forms import *
from django.conf import settings


class ExitView(View):
    def get(self, request, *args, **kwargs):
        self.request.session.flush()
        return redirect('webinterface:atm')


class AtmView(FormView):
    template_name = 'webinterface/atm.html'
    form_class = CardNumForm
    success_url = "/atm/pin"

    def form_valid(self, request, *args, **kwargs):
        nm = self.request.POST['number'].replace(" ", "")
        hashed = make_password(nm, salt=settings.SECRET_KEY)
        search_results = Card.objects.filter(hashed=hashed)
        if not search_results:
            return redirect('webinterface:error', error='NOT_FOUND')
        if search_results[0].pin_block:
            return redirect('webinterface:error', error='BLOCKED')
        self.request.session['key'] = search_results[0].id
        return super(AtmView, self).form_valid(request, *args, **kwargs)


class WithdrawView(FormView):
    template_name = 'webinterface/withdraw.html'
    form_class = WithdrawForm
    success_url = "/atm/result"

    def get(self, request, *args, **kwargs):
        if not self.request.session.get('auth', None):
            return redirect('webinterface:error', error='NOTAUTH')
        return super(WithdrawView, self).get(request, *args, **kwargs)

    def form_valid(self, request, *args, **kwargs):
        sum = int(self.request.POST['sum'])
        id = self.request.session['key']
        card = Card.objects.get(id=id)
        if card.balance < sum:
            return redirect('webinterface:error', error='EXCESS')
        else:
            card.balance -= sum
            Card.objects.filter(id=id).update(balance=card.balance)
            op = Operations(card=card.number, code='WITHDRAW', sum=sum)
            op.save()
            return redirect('webinterface:balance')


class PinView(FormView):
    template_name = 'webinterface/pin.html'
    form_class = CardPinForm
    success_url = "/atm/operations"

    def form_valid(self, request, *args, **kwargs):
        pin = self.request.POST['pin']
        hashed = make_password(pin, salt=settings.SECRET_KEY)
        id = self.request.session['key']
        card = Card.objects.get(id=id)
        if card.pin != hashed:
            print(card.pin_attempts)
            print(card.pin_block)
            attempts = card.pin_attempts
            if attempts < 4:
                attempts += 1
                Card.objects.filter(id=id).update(pin_attempts=attempts)
                return redirect('webinterface:error', error='BADPIN')
            else:
                Card.objects.filter(id=id).update(pin_block=True)
                return redirect('webinterface:error', error='BLOCKED')
        self.request.session['auth'] = True
        return super(PinView, self).form_valid(request, *args, **kwargs)


class ErrorView(TemplateView):
    template_name = 'webinterface/error.html'

    def get_context_data(self, **kwargs):
        context = super(ErrorView, self).get_context_data(**kwargs)
        return context

class OperationsView(TemplateView):
    template_name = 'webinterface/operations.html'

    def get(self, request, *args, **kwargs):
        if not self.request.session.get('auth', None):
            return redirect('webinterface:error', error='NOTAUTH')
        return super(OperationsView, self).get(request, *args, **kwargs)


class BalanceView(TemplateView):
    template_name = 'webinterface/balance.html'

    def get(self, request, *args, **kwargs):
        if not self.request.session.get('auth', None):
            return redirect('webinterface:error', error='NOTAUTH')
        id = self.request.session['key']
        card = Card.objects.get(id=id)
        op = Operations(card=card.number, code='OP_VIEW')
        op.save()
        return super(BalanceView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(BalanceView, self).get_context_data()
        id = self.request.session['key']
        card = Card.objects.get(id=id)
        context['number'] = card.number
        context['balance'] = card.balance
        return context