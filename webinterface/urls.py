from django.conf.urls import url

from webinterface.views import *


urlpatterns = [
    url(r'^$', AtmView.as_view(), name='atm'),
    url(r'^pin/$', PinView.as_view(), name='pin'),
    url(r'^operations/$', OperationsView.as_view(), name='operations'),
    url(r'^error/(?P<error>\w+)', ErrorView.as_view(), name='error'),
    url(r'^quit/$', ExitView.as_view(), name='exit'),
    url(r'^balance/$', BalanceView.as_view(), name='balance'),
    url(r'^withdraw/$', WithdrawView.as_view(), name='withdraw'),
]