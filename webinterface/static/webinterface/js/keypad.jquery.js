$(document).ready(function(e) {
    $('#1,#2,#3,#4,#5,#6,#7,#8,#9,#0').click(function(){
        var v = $(this).html();
        var maxlen = $('.form-control').attr("maxlength");
        var len = $('.form-control').val().replace(/ /g,'').length;
        var id = $('.form-control').attr('id')
        if (len < maxlen) {
            if (len % 4 == 0 && len != 0 && id != "sum") {
                $('.form-control').val($('.form-control').val() + ' ' + v);
            } else {
                $('.form-control').val($('.form-control').val() + v);
            }
        }
    });
    $('#C').click(function(){
        $('.form-control').val('');
    });
});