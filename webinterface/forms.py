from django import forms


class CardNumForm(forms.Form):
    number = forms.CharField()


class CardPinForm(forms.Form):
    pin = forms.CharField(widget=forms.PasswordInput)


class WithdrawForm(forms.Form):
    sum = forms.CharField()